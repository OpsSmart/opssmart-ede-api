# OpsSmart EDE Document

The documentation is made to describe real-time Data Exchange Methodolgy by using API technology in OpsSmart EDE module.

## Method POST

### Request

**Authorization** Bearer Token

| Key | Value |
| ------ | ------ |
| Token | xxx |

`Token` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; encrypted Bearer Token provided by OpsSmart (OpsSmart API Token)

**Request Headers**

| Key | Value |
| ------ | ------ |
| UserName | xxx |
| Password | xxx |
| APIFormat | JSON or XML |

`UserName` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart API User Name who registered in OpsSmart

`Password` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart API Password

`APIFormat` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; schema type of request body can be only `JSON` for now

## EXPORT PROCESS
### Step 1: Initialization
In order to start the process you will perform the following HTTP Request to vertify authentication:
```json
HTTP Post 1.1
{OpsSmart-API-URL}/EDEAPI/Authen

Accept          application/json
Content-Type    application/json
Token           xxx
UserName        xxx
Password        xxx
APIFormat       JSON

{
    "CustomerCode" :  "xxxx"
}   
```

This HTTP Request will return the following response:
```json
Content-Type    application/json
{
    "Success": true,
    "Message": "Authentication succeeded."
}
```
### Step 2: Request Export Data Capsule(s)
In order to start the process you will perform the following HTTP Request to get all OpsSmart Export Data Capsules:
```json
HTTP Post 1.1
{OpsSmart-API-URL}/EDEAPI/getExportDataCapsule

Accept          application/json
Content-Type    application/json
UserName        xxx
Password        xxx
APIFormat       JSON

{
    "MODE" :  "opssmart",
    "EXP_IMP_FLAG" :  "E",
    "STATUS" :  "PUBLISHED",
    "CapsuleType": "JSON",
    "CustomerCode": "xxxx"
}   
```

This HTTP Request will return the following response:
```json
Content-Type    application/json
[
    {
        "CapsuleName": "BR-OMN_FarmMaster_JSON_v1",
        "CapsuleType": "JSON",
        "SearchCriteria": {
            "EntityCodeFrom": "",
            "EntityCodeTo": "",
            "EntityNameFrom": "",
            "EntityNameTo": "",
            "Time_Stamp_From": "",
            "Time_Stamp_To": ""
        },
        "ExportMode": ""
    }
    {
        "CapsuleName": "BR-OMN_FarmMaster_JSON_v2",
        "CapsuleType": "JSON",
        "SearchCriteria": {
            "EntityCodeFrom": "",
            "EntityCodeTo": "",
            "EntityNameFrom": "",
            "EntityNameTo": "",
            "EntityParentCodeFrom": "",
            "EntityParentCodeTo": "",
            "EntityParentNameFrom": "",
            "EntityParentNameTo": "",
            "Time_Stamp_From": "",
            "Time_Stamp_To": ""
        },
        "ExportMode": ""
    }
] 
```
> ***Note:*** Search Criteria Attribute(s) will be listed according to Data Capsule configuration. Time_Stamp_From and Time_Stamp_To to be filtered at Records TimeStamp.

### Step 3: Export Data from OpsSmart
In order to start the Export process you will perform the following HTTP Request:
```json
HTTP Post 1.1
{OpsSmart-API-URL}/EDEAPI/Export

Accept          application/json
Content-Type    application/json
UserName        xxx
Password        xxx
APIFormat       JSON

[
    {
        "CapsuleName": "BR-OMN_FarmMaster_JSON_v1",
        "CapsuleType": "JSON",
        "SearchCriteria": {
            "EntityCodeFrom": "0810000001",
            "EntityCodeTo": "0810000001",
            "EntityNameFrom": "",
            "EntityNameTo": "",
            "Time_Stamp_From": "",
            "Time_Stamp_To": ""
        },
        "ExportMode": "OPEN"
    }
    {
        "CapsuleName": "BR-OMN_FarmMaster_JSON_v2",
        "CapsuleType": "JSON",
        "SearchCriteria": {
            "EntityCodeFrom": "0810000001",
            "EntityCodeTo": "0810000001",
            "EntityNameFrom": "",
            "EntityNameTo": "",
            "EntityParentCodeFrom": "0500014561",
            "EntityParentCodeTo": "0500014561",
            "EntityParentNameFrom": "",
            "EntityParentNameTo": "",
            "Time_Stamp_From": "",
            "Time_Stamp_To": ""
        },
        "ExportMode": "OPEN"
    }
    {
        "CapsuleName": "BR-OMN_FarmMaster_JSON_v3",
        "CapsuleType": "JSON",
        "SearchCriteria": {
            "EntityCodeFrom": "",
            "EntityCodeTo": "",
            "EntityNameFrom": "",
            "EntityNameTo": "",
            "Time_Stamp_From": "2024-12-22 00:00",
            "Time_Stamp_To": "2024-12-22 23:59"
        },
        "ExportMode": "ALL"
    }
]     
```
> ***Note:*** ExportMode (OPEN) for Open records. (ALL) for both Open and Terminated records.

This HTTP Request will return the following response:
```json
Content-Type    application/json
{
    "Document": {
        "ResponseDateTime": "2025-02-17 17:55:25:802",
        "Sender": "DEVADMIN",
        "CapsuleFileName": "BGBR-01_DC3230.txt",
        "SessionID": "2y5o5crb3imc4ihfqfsleawf",
        "ActionType": "REALTIME",
        "DataObject": [
            {
                "ObjectName": "Farmer UDF",
                "ObjectType": "SINGLE",
                "TemplateName": "TP_BR-OMN_FarmMaster_S",
                "AttributeProperties": {
                    "Attribute": [
                        {
                            "Name": "Status",
                            "Type": "STRING",
                            "MaxLength": "30"
                        },
                        {
                            "Name": "Entity Code",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "Entity Name",
                            "Type": "STRING",
                            "MaxLength": "249.0"
                        },
                        {
                            "Name": "Entity Parent Code",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "Entity Parent Name",
                            "Type": "STRING",
                            "MaxLength": "249.0"
                        },                        
                        {
                            "Name": "Effective Date",
                            "Type": "DATE",
                            "MaxLength": "50.0",
                            "Format": "yyyy-MM-dd"
                        },
                        {
                            "Name": "Discontinue Date",
                            "Type": "DATETIME",
                            "MaxLength": "50.0",
                            "Format": "yyyy-MM-dd HH:mm"
                        },
                        {
                            "Name": "TimeIn",
                            "Type": "TIME",
                            "Format": "HH:mm",
                            "MaxLength": "50.0"
                        },                                                                  
                        {
                            "Name": "Farm Picture",
                            "Type": "ATTACHMENT",
                            "MaxLength": "249.0"
                        },
                        {
                            "Name": "Farm Area",
                            "Type": "NUMBER",
                            "MaxLength": "12.2",
                            "Format": "###0.00"
                        },
                        {
                            "Name": "Total House",
                            "Type": "NUMBER",
                            "MaxLength": "12.0",
                            "Format": "###0"
                        }
                        
                    ]
                },
                "DataSets": {
                    "DataRecord": [
                        {
                            "RowID": 1,
                            "Attributes": {
                                "Attribute": [
                                    {
                                        "Name": "Status",
                                        "Value": "OPEN"
                                    },
                                    {
                                        "Name": "Entity Code",
                                        "Value": "0810000001"
                                    },
                                    {
                                        "Name": "Entity Name",
                                        "Value": "ABCD"
                                    },
                                    {
                                        "Name": "Entity Parent Code",
                                        "Value": "0500014561"
                                    },
                                    {
                                        "Name": "Entity Parent Name",
                                        "Value": "FGHIJ"
                                    },                                    
                                    {
                                        "Name": "Effective Date",
                                        "Value": "2022-02-07"
                                    },
                                    {
                                        "Name": "Discontinue Date",
                                        "Value": "2030-12-30"
                                    },
                                    {
                                        "Name": "TimeIn",
                                        "Format": "09:00"
                                    },                                    
                                    {
                                        "Name": "Farm Picture",
                                        "Value": "CUST-01_20150901092219820.jpg"
                                    },
                                    {
                                        "Name": "Farm Area",
                                        "Value": "50.00"
                                    },
                                    {
                                        "Name": "Total House",
                                        "Value": "9"
                                    }
                                ]
                            },
                            "RecordResponseCode": "0000",
                            "RecordResponseDesc": "Successfully Imported."
                        }
                    ]
                }
            }
        ],
        "Contents": {
            "Attribute": [
                {
                    "Name": "CUST-01_20150901092219820.jpg",
                    "Type": "jpg",
                    "MimeType": "image/jpeg",
                    "Value": "/9j/4AAQSkZJRgABAgEAYABgAAD/4RfURXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAIAA..."
                }
            ]
        },
        "ResponseCode": "0000",
        "ResponseDesc": "Successfully Exported."
    }
}
```
## IMPORT PROCESS
### Step 1: Initialization
In order to start the process you will perform the following HTTP Request to vertify authentication:
```json
HTTP Post 1.1
{OpsSmart-API-URL}/EDEAPI/Authen

Accept          application/json
Content-Type    application/json
Token           xxx
UserName        xxx
Password        xxx
APIFormat       JSON

{
    "CustomerCode" :  "xxxx"
}   
```

This HTTP Request will return the following response:
```json
Content-Type    application/json
{
    "Success": true,
    "Message": "Authentication succeeded."
}
```

### Step 2: Request Import DC (Option 1)
In order to start the process you will perform the following HTTP Request to get all OpsSmart Import Data Capsules:
```json
HTTP Post 1.1
{OpsSmart-API-URL}/EDEAPI/getImportDataCapsule

Accept          application/json
Content-Type    application/json
UserName        xxx
Password        xxx
APIFormat       JSON

{
    "MODE" :  "legacy",
    "EXP_IMP_FLAG" :  "I",
    "STATUS" :  "PUBLISHED",
    "CapsuleType": "JSON"
}   
```

This HTTP Request will return the following response:
```json
Content-Type    application/json
[
    {
        "CapsuleName": "FT-FactoryReceipt_JSON",
        "CapsuleType": "JSON"
    },
    {
        "CapsuleName": "FT-Shipment_JSON",
        "CapsuleType": "JSON"
    }
] 
```
### Step 2: Request Import DC (Option 2)
Another option to get OpsSmart Import Data Capsules by OpsSmart Batch, you will perform the following HTTP Request:
```json
HTTP Post 1.1
{OpsSmart-API-URL}/EDEAPI/getDataCapsuleByBatch

Accept          application/json
Content-Type    application/json
UserName        xxx
Password        xxx
APIFormat       JSON

{
    "BatchNames" : [ 
            "Import_Batch_By_API"
        ]
}     
```
> ***Note:*** User have to create Import Batch in OpsSmart system before this request.

This HTTP Request will return the following response:
```json
Content-Type    application/json
[
    {
        "CapsuleName": "FT-FactoryReceipt_JSON",
        "CapsuleType": "JSON"
    },
    {
        "CapsuleName": "FT-Shipment_JSON",
        "CapsuleType": "JSON"
    }
] 
```

> ***Note:*** Skip Step 2 to Request specific Data Capsule

### Step 3: Request Import Data Capsule JSON Template
In order to get OpsSmart Data Capsule JSON Template you will perform the following HTTP Request:
```json
HTTP Post 1.1
{OpsSmart-API-URL}/EDEAPI/getTemplatebyDC

Accept          application/json
Content-Type    application/json
UserName        xxx
Password        xxx
APIFormat       JSON

[
    {
        "CapsuleName": "FT-FactoryReceipt_JSON",
        "CapsuleType": "JSON"
    },
    {
        "CapsuleName": "FT-Shipment_JSON",
        "CapsuleType": "JSON"
    }
]     
```

<details><summary markdown="span">This HTTP Request will return the following response: (Click here)</summary>

```json
[
    {
        "Document": {
            "RequestDateTime": "2023-09-08 10:11:37:169",
            "Sender": "SenderName",
            "SYS_ORGN_CODE": "AF-TESCO-01",
            "SYS_ENTITYTYPE_CODE": "AF-TESCO-01",
            "SYS_USER_CODE": "DevAdmin-01",
            "SYS_ENTITYTYPE_NAME": "ORGANIZATION",
            "CapsuleFileName": "OPS-DEMO_DC22.txt",
            "CapsuleName": "FT-FactoryReceipt_JSON",
            "ActionType": "REALTIME",
            "DataObject": [
                {
                    "ObjectID": "107",
                    "ObjectName": "FactoryReceiptDo",
                    "ObjectType": "SINGLE",
                    "TemplateName": "FT-FactoryReceipt_S_JSON",
                    "AttributeProperties": {
                        "Attribute": [
                            {
                                "Name": "Status",
                                "Type": "STRING",
                                "MaxLength": "30"
                            },
                            {
                                "Name": "LicenseCompanyCode",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "FactoryReceiptDate",
                                "Type": "DATE",
                                "Format": "yyyy-MM-dd",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "FactoryCode",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "FactoryName",
                                "Type": "STRING",
                                "MaxLength": "249.0"
                            },
                            {
                                "Name": "ItemCode",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "ItemName",
                                "Type": "STRING",
                                "MaxLength": "249.0"
                            },
                            {
                                "Name": "RawMaterialLot",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "RawMaterialGrade",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "TotalWeight",
                                "Type": "NUMBER",
                                "Format": "###0.00",
                                "MaxLength": "10.2"
                            },
                            {
                                "Name": "UOM-Weight",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "Quantity",
                                "Type": "NUMBER",
                                "Format": "###0",
                                "MaxLength": "10.0"
                            },
                            {
                                "Name": "UOM-Quantity",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            }
                        ]
                    },
                    "TotalRecord": "0",
                    "DataSets": {
                        "DataRecord": []
                    }
                }
            ],
            "Contents": {
                "Attribute": []
            }
        }
    },
    {
        "Document": {
            "RequestDateTime": "2023-09-08 10:11:36:709",
            "Sender": "SenderName",
            "SYS_ORGN_CODE": "AF-TESCO-01",
            "SYS_ENTITYTYPE_CODE": "AF-TESCO-01",
            "SYS_USER_CODE": "DevAdmin-01",
            "SYS_ENTITYTYPE_NAME": "ORGANIZATION",
            "CapsuleFileName": "OPS-DEMO_DC20.txt",
            "CapsuleName": "FT-Shipment_JSON",
            "ActionType": "REALTIME",
            "DataObject": [
                {
                    "ObjectID": "108",
                    "ObjectName": "ShipmentDO",
                    "ObjectType": "SINGLE",
                    "TemplateName": "FT-Shipment_S_JSON",
                    "AttributeProperties": {
                        "Attribute": [
                            {
                                "Name": "Status",
                                "Type": "STRING",
                                "MaxLength": "30"
                            },
                            {
                                "Name": "LicenseCompanyCode",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "QGMP",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "ManufacturerLocation",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "ShipmentDate",
                                "Type": "DATE",
                                "Format": "yyyy-MM-dd",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "InvoiceNo",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "CustomerCode",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "CustomerName",
                                "Type": "STRING",
                                "MaxLength": "249.0"
                            },
                            {
                                "Name": "ExporterCode",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "ExporterName",
                                "Type": "STRING",
                                "MaxLength": "249.0"
                            },
                            {
                                "Name": "FlightNo",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "ContainerNo",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "PortofEntry",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            }
                        ]
                    },
                    "TotalRecord": "0",
                    "DataSets": {
                        "DataRecord": []
                    }
                },
                {
                    "ObjectID": "117",
                    "ObjectName": "ShipmentDetails",
                    "ObjectType": "MULTI",
                    "TemplateName": "FT-Shipment_M_JSON",
                    "AttributeProperties": {
                        "Attribute": [
                            {
                                "Name": "Status",
                                "Type": "STRING",
                                "MaxLength": "30"
                            },
                            {
                                "Name": "PackingDate",
                                "Type": "DATE",
                                "Format": "yyyy-MM-dd",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "ProductCode",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "ProductName",
                                "Type": "STRING",
                                "MaxLength": "249.0"
                            },
                            {
                                "Name": "LotSerialNo",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "DisplayLotNo",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "Quantity",
                                "Type": "NUMBER",
                                "Format": "###0.00",
                                "MaxLength": "10.2"
                            },
                            {
                                "Name": "UOM",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "Weight",
                                "Type": "NUMBER",
                                "Format": "###0.00",
                                "MaxLength": "10.2"
                            },
                            {
                                "Name": "LicenseCompanyCode",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "ShipmentDate",
                                "Type": "DATE",
                                "Format": "yyyy-MM-dd",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "InvoiceNoMV",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            },
                            {
                                "Name": "CustomerCode",
                                "Type": "STRING",
                                "MaxLength": "50.0"
                            }
                        ]
                    },
                    "TotalRecord": "0",
                    "DataSets": {
                        "DataRecord": []
                    }
                }
            ],
            "Contents": {
                "Attribute": []
            }
        }
    }
]
```
</details>

### Step 4.1: Import Data to OpsSmart
In order to strat the Import process you will perform the following HTTP Request:

> ***Note:*** You may need to import ONE Data Capsule at a time. The following example show to import FT-FactoryReceipt_JSON template with Single Value section.

```json
HTTP Post 1.1
{OpsSmart-API-URL}/EDEAPI/getTemplatebyDC

Accept          application/json
Content-Type    application/json
UserName        xxx
Password        xxx
APIFormat       JSON

{
    "Document": {
        "RequestDateTime": "2023-09-08 10:11:37:169",
        "Sender": "SenderName",
        "SYS_ORGN_CODE": "AF-TESCO-01",
        "SYS_ENTITYTYPE_CODE": "AF-TESCO-01",
        "SYS_USER_CODE": "DevAdmin-01",
        "SYS_ENTITYTYPE_NAME": "ORGANIZATION",
        "CapsuleFileName": "OPS-DEMO_DC22.txt",
        "CapsuleName": "FT-FactoryReceipt_JSON",
        "ActionType": "REALTIME",
        "DataObject": [
            {
                "ObjectID": "107",
                "ObjectName": "FactoryReceiptDo",
                "ObjectType": "SINGLE",
                "TemplateName": "FT-FactoryReceipt_S_JSON",
                "AttributeProperties": {
                    "Attribute": [
                        {
                            "Name": "Status",
                            "Type": "STRING",
                            "MaxLength": "30"
                        },
                        {
                            "Name": "LicenseCompanyCode",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "FactoryReceiptDate",
                            "Type": "DATE",
                            "Format": "yyyy-MM-dd",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "RawMaterialLot",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "RawMaterialGrade",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "TotalWeight",
                            "Type": "NUMBER",
                            "Format": "###0.00",
                            "MaxLength": "10.2"
                        }
                    ]
                },
                "TotalRecord": "1",
                "DataSets": {
                    "DataRecord": [
                        {
                            "RowID": 1,
                            "Attributes": {
                                "Attribute": [
                                    {
                                        "Name": "Status",
                                        "Value": "OPEN"
                                    },
                                    {
                                        "Name": "LicenseCompanyCode",
                                        "Value": "AF-TESCO-01"
                                    },
                                    {
                                        "Name": "FactoryReceiptDate",
                                        "Value": "2023-09-08"
                                    },
                                    {
                                        "Name": "RawMaterialLot",
                                        "Value": "RM0001"
                                    },
                                    {
                                        "Name": "RawMaterialGrade",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "TotalWeight",
                                        "Value": "100"
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        ],
        "Contents": {
            "Attribute": []
        }
    }
}
```

### Step 4.2: Import Data to OpsSmart
In order to strat the Import process you will perform the following HTTP Request:

> ***Note:***  You will only allow to import ONE Data Capsule at a time. The following example show to import FT-Shipment_JSON template with Single Value and Multivalue.

```json
HTTP Post 1.1
{OpsSmart-API-URL}/EDEAPI/IMPORT

Accept          application/json
Content-Type    application/json
UserName        xxx
Password        xxx
APIFormat       JSON

{
    "Document": {
        "RequestDateTime": "2023-09-08 10:11:36:709",
        "Sender": "SenderName",
        "SYS_ORGN_CODE": "AF-TESCO-01",
        "SYS_ENTITYTYPE_CODE": "AF-TESCO-01",
        "SYS_USER_CODE": "DevAdmin-01",
        "SYS_ENTITYTYPE_NAME": "ORGANIZATION",
        "CapsuleFileName": "OPS-DEMO_DC20.txt",
        "CapsuleName": "FT-Shipment_JSON",
        "ActionType": "REALTIME",
        "DataObject": [
            {
                "ObjectID": "108",
                "ObjectName": "ShipmentDO",
                "ObjectType": "SINGLE",
                "TemplateName": "FT-Shipment_S_JSON",
                "AttributeProperties": {
                    "Attribute": [
                        {
                            "Name": "Status",
                            "Type": "STRING",
                            "MaxLength": "30"
                        },
                        {
                            "Name": "LicenseCompanyCode",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "QGMP",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "ManufacturerLocation",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "ShipmentDate",
                            "Type": "DATE",
                            "Format": "yyyy-MM-dd",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "InvoiceNo",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "CustomerCode",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "CustomerName",
                            "Type": "STRING",
                            "MaxLength": "249.0"
                        },
                        {
                            "Name": "ExporterCode",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "ExporterName",
                            "Type": "STRING",
                            "MaxLength": "249.0"
                        },
                        {
                            "Name": "FlightNo",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "ContainerNo",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "PortofEntry",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        }
                    ]
                },
                "TotalRecord": "0",
                "DataSets": {
                    "DataRecord": []
                }
            },
            {
                "ObjectID": "117",
                "ObjectName": "ShipmentDetails",
                "ObjectType": "MULTI",
                "TemplateName": "FT-Shipment_M_JSON",
                "AttributeProperties": {
                    "Attribute": [
                        {
                            "Name": "Status",
                            "Type": "STRING",
                            "MaxLength": "30"
                        },
                        {
                            "Name": "PackingDate",
                            "Type": "DATE",
                            "Format": "yyyy-MM-dd",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "ProductCode",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "ProductName",
                            "Type": "STRING",
                            "MaxLength": "249.0"
                        },
                        {
                            "Name": "LotSerialNo",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "DisplayLotNo",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "Quantity",
                            "Type": "NUMBER",
                            "Format": "###0.00",
                            "MaxLength": "10.2"
                        },
                        {
                            "Name": "UOM",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "Weight",
                            "Type": "NUMBER",
                            "Format": "###0.00",
                            "MaxLength": "10.2"
                        },
                        {
                            "Name": "LicenseCompanyCode",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "ShipmentDate",
                            "Type": "DATE",
                            "Format": "yyyy-MM-dd",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "InvoiceNoMV",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "CustomerCode",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        }
                    ]
                },
                "TotalRecord": "0",
                "DataSets": {
                    "DataRecord": []
                }
            }
        ],
        "Contents": {
            "Attribute": []
        }
    }
}
```
This HTTP Request will return the following response:
### Response
```json
{
    "Document": {
        "ResponseDateTime": "2023-09-08 15:50:55:945",
        "Sender": "SenderName",
        "CapsuleFileName": "OPS-DEMO_DC22.txt",
        "ActionType": "REALTIME",
        "DataObject": [
            {
                "ObjectName": "FactoryReceiptDo",
                "ObjectType": "SINGLE",
                "TemplateName": "FT-FactoryReceipt_S_JSON",
                "AttributeProperties": {
                    "Attribute": [
                        {
                            "Name": "Status",
                            "Type": "STRING",
                            "MaxLength": "30"
                        },
                        {
                            "Name": "LicenseCompanyCode",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "FactoryReceiptDate",
                            "Type": "DATE",
                            "MaxLength": "50.0",
                            "Format": "yyyy-MM-dd"
                        },
                        {
                            "Name": "RawMaterialLot",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "RawMaterialGrade",
                            "Type": "STRING",
                            "MaxLength": "50.0"
                        },
                        {
                            "Name": "TotalWeight",
                            "Type": "NUMBER",
                            "MaxLength": "10.2",
                            "Format": "###0.00"
                        }
                    ]
                },
                "DataSets": {
                    "DataRecord": [
                        {
                            "RowID": 1,
                            "Attributes": {
                                "Attribute": [
                                    {
                                        "Name": "Status",
                                        "Value": "OPEN"
                                    },
                                    {
                                        "Name": "LicenseCompanyCode",
                                        "Value": "AF-TESCO-01"
                                    },
                                    {
                                        "Name": "FactoryReceiptDate",
                                        "Value": "2023-09-08"
                                    },
                                    {
                                        "Name": "RawMaterialLot",
                                        "Value": "RM0002"
                                    },
                                    {
                                        "Name": "RawMaterialGrade",
                                        "Value": ""
                                    },
                                    {
                                        "Name": "TotalWeight",
                                        "Value": ""
                                    }
                                ]
                            },
                            "RecordResponseCode": "0000",
                            "RecordResponseDesc": "Successfully Imported."
                        }
                    ]
                }
            }
        ],
        "Contents": {
            "Attribute": []
        },
        "ResponseCode": "0000",
        "ResponseDesc": "Successfully Imported."
    }
}
```

### Understanding OpsSmart JSON template
`Document` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of all Objects

`RequestDateTime` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Datetime while sending the request, format `yyyy/MM/dd HH:mm:ss:fff`

`Sender` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; User Name of Sender

`SYS_ORGN_CODE` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; SYS_ORGN_CODE

`SYS_ENTITYTYPE_CODE` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; SYS_ENTITYTYPE_CODE

`SYS_USER_CODE` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; SYS_USER_CODE

`SYS_ENTITYTYPE_NAME` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; SYS_ENTITYTYPE_NAME

`CapsuleFileName` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Import Data Capsule FILE_NAME

`SessionID` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Browser or Program session

`ActionType` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Fixed Value (REALTIME)

`DataObject` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain detial of DDOT and Template

`ObjectID` (optional)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart Data Object ID

`ObjectName` (optional)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart Data Object Name

`ObjectType` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Signle or Multi Value DataSets

`TemplateName` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Data Capsule Template Name

`AttributeProperties` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain details of every attributes in the template

`Attribute` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain details of the attribute

`Name` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart Attribute ATTR_NAME

`Type` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart Attribute DATA_TYPE

`Format` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Required if Type is DATE or NUMBER

`MaxLength` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart Attribute MAX_LENGTH

`TotalRecord` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Total Record to import

`DataSets` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of DataRecord

`DataRecord`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain detial of every records in the template

`RowID`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Record Row Number

`Attribute`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Name = "OpsSmart Attribute ATTR_NAME", Value = Actual Data

`Contents` (optional)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain details of every attachment attributes

`Name` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Attachment Value (e.g. "Plant Picture Value = AP-Betag-10_20150925173926247.jpg")

`Type`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Attachment Data Format (e.g. ".jpg", ".docx", ".xlsx")

`Value`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; String base 64 (e.g. "data:image/pjpeg;base64,/9j/4AAQSkZJRgABAQEAMwA0AAD/4QAWRXhpZgAASUkqAAgAAAAAAAAAAA..")


**Response Code**

| RESPONSE_CODE | DESCRIPTION |
| ------ | ------ |
| 0000 | Process successfully completed. |
| EDE1000 | Export JSON data file downloaded. |
| EDE1001 | Data Capsule not found : [[VALUE]] |
| EDE1002 | SQL syntax error and/or No Records found : [[VALUE]] |
| EDE1003 | Parameter or RequestBody is null. |
| EDE1004 | Sent Request failed : [[VALUE]] |
| EDE1005 | Filetransfer: Data Transfer Successfully for Customer : [[VALUE]] |
| EDE1006 | Filetransfer: Data Transfer Failed for Customer : [[VALUE]] |
| EDE1007 | No record found to export. |
| EDE1008 | Incorrect Data Capsule or Template configuration. |
| EDE2001 | Authentication failed. Invalid API Username, Password or Token. |
| EDE2002 | Authentication failed. The API Token expired. |
| EDE2003 | Invalid API Request message format. Failed to retrieve Data Capsule. FILE_NAME = [[FILE_NAME]] |
| EDE2004 | Invalid API Request message format. Failed to retrieve Data Capsule Template. TEMPLATE_NAME = [[TEMPLATE_NAME]] |
| EDE2005 | Invalid API Request message format. |
| EDE2006 | Mandatory field required. |
| EDE2010 | DataSet [[TEMPLATE_NAME]] data does not match the Template format |
| EDE2011 | 1.	Check Logical Expression Fail in -><br />2.	Validation failure due to missing reference of Multivalue section record to unique key attribute of Single value section for Record No: "": Unique Key failed: "": Values: ""<br />3.	Maxlength of data more than defined for Record No: “”: Value: “”<br />4.	Invalid data type values for Record No: “”: Value: “”<br />5.	Data set does not match with the template set: Line No:<br />6.	Customer code Validation Failed<br />7.	Unique Fail-Null Value<br />8.	STATUS value failed<br />9.	Special character \| present in Line No<br />10.	Special character ~ present in Line No<br />11.	Enum value failed<br />12.	Single value Lookup failed<br />13.	Multi value Lookup failed<br />14.	Uniqueness failed at file level<br />15.	Uniqueness failed<br />16.	Unique Key failed<br />17.	Unique value set not available for the multi value section<br />18.	Fill lookup failed.<br /> |
| EDE3000 | Import failed. Missing SYS_USER_CODE in Data Capsule. |
| EDE8000 | Some records failed to import. |
| EDE9000 | No record found. TEMPLATE_NAME = [[TEMPLATE_NAME]] |
| EDE9999 | Error in Import process. |
```
